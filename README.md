# Continous Integration and Continous Deployment (CICD) using Mule Maven Plugin
The Complete step by step guide to incorporate CICD process.

# Prerequisites
# Changes required in setting.xml
    ![Add_PluginGroup_in_setting.xml](Images/PluginGroups.png)
    
    ![Add Profile in setting.xml](Images/Profiles.png)

**Note: you can find the sample setting.xml file under maven_home/conf directory**

# Changes required in project project object model i.e. pom.xml
    ![Add plugin in pom.xml](Images/pom-plugin.xml.png)
     
**Note: Value present under the properties section will be displayed in cloudhub properties section.**

# Command line
    mvn clean package deploy -DmuleDeploy -Danypoint.account.username=<<username>> -Danypoint.account.password=<<password>> -Dmule.version=<<Version>> -Danypoint.runtime.environment=<<EnvName>> -Ddeploy.applicationName=<<Unique_App_Name>> -Dapp.env=<<Value>>

**In case you want to debug, run the above command with *-e –X* option**
